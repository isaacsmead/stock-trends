import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.DataFrame
object StockTrends {

  def main (args: Array[String]){

    if(args.length < 2){
      print("Error, args(0)=<< input file >> << output file >>")
      System.exit(1)
    }

    // uncomment to run in intellij
    /*/////////////////////////////////////////////
    val spark = SparkSession
      .builder()
      .appName("stock-trends")
      .config("spark.master", "local")
      .getOrCreate()

    spark.sparkContext.setLogLevel("ERROR")
    *////////////////////////////////////////////////

    // uncomment to make JAR
    //*///////////////////////////////////////////////
    val spark = SparkSession.builder().appName("stock-trends").getOrCreate()
    Logger.getLogger("org").setLevel(Level.ERROR)
    //*////////////////////////////////////////////////


    val rawData = spark.read
      .option("header","true")
      .option("inferschema", "true")
      .csv(args(0))

    // ~252 trading days per year https://en.wikipedia.org/wiki/Trading_day
    // window the data to get 52wk HIGH/LOW, %above 30 60 and 90 day ma, annual ave Volume
    // id,Date,Open,High,Low,Close,Volume,OpenInt
    val partition= Window.partitionBy("id").orderBy("Date")
    val annual = partition.rowsBetween(-251, 0)
    val _30day = partition.rowsBetween(-29, 0)
    val _60day = partition.rowsBetween(-59, 0)
    val _90day = partition.rowsBetween(-89, 0)
    val windowed = rawData
      .withColumn("52High", max("High").over(annual))
      .withColumn("52Low", min("Low").over(annual))
      .withColumn("aveVol", mean("Volume").over(annual))
      .withColumn("MA30", mean("Close").over(_30day))
      .withColumn("MA60", mean("Close").over(_60day))
      .withColumn("MA90", mean("Close").over(_90day))
      .withColumn("toDrop", lag("52High", 251, null).over(partition))

    // filter out the "toDrop" rows, 52 week data point needs a full year of
    // data to be accurate
    val cleaned = windowed.filter(col("toDrop").isNotNull)

    // with windowed columns add boolian columns if at at/near 52 high / low
    // and if price gain/loss is accelerating
    // id,Date,Open,High,Low,Close,Volume,OpenInt,52High,52Low,aveVol,MA30,MA60,MA90
    val withBools = cleaned
        .withColumn("at52High", col("High").equalTo(col("52High")))
        .withColumn("at52Low", col("Low").equalTo(col("52Low")))
        .withColumn("nearHighBy5", (col("52High").minus(col("High"))).divide(col("52High")).lt(.05))
        .withColumn("nearLowBy5", (col("Low").minus(col("52Low"))).divide(col("52Low")).lt(.05))
        .withColumn("nearHighBy3", (col("52High").minus(col("High"))).divide(col("52High")).lt(.03))
        .withColumn("nearLowBy3", (col("Low").minus(col("52Low"))).divide(col("52Low")).lt(.03))
        .withColumn("accPrice",
          (col("Close").gt(col("MA30")))
            .and(col("MA30").gt(col("MA60")))
            .and((col("Close").minus(col("MA30")).gt(col("MA30").minus(col("MA60"))))))
        .withColumn("decPrice",
          (col("Close").lt(col("MA30")))
          .and(col("MA30").lt(col("MA60")))
          .and((col("MA30").minus(col("Close")).gt(col("MA60").minus(col("MA30"))))))



    // aggregate the data down by counting occurances of various booleans
    val grouped = withBools.groupBy("Date")
        .agg(
          count(when(col("at52High"),true)).alias("countAt52High"),
          count(when(col("at52Low"),true)).alias("countAt52Low"),
          count(when(col("nearHighBy5"),true)).alias("countNearHighBy5"),
          count(when(col("nearLowBy5"),true)).alias("countNearLowBy5"),
          count(when(col("nearHighBy3"),true)).alias("countNearHighBy3"),
          count(when(col("nearLowBy3"),true)).alias("countNearLowBy3"),
          count(when(col("accPrice"),true)).alias("countPriceAcc"),
          count(when(col("decPrice"),true)).alias("countPriceDec"),
          count(col("Date")).alias("groupSize"))

    // add in columns with corresponding percentages for counts
    val groupedWithPct = grouped
      .withColumn("pctAt52High", col("countAt52High").divide(col("groupSize")))
      .withColumn("pctAt52Low", col("countAt52Low").divide(col("groupSize")))
      .withColumn("pctNearHighBy5", col("countNearHighBy5").divide(col("groupSize")))
      .withColumn("pctNearLowBy5", col("countNearLowBy5").divide(col("groupSize")))
      .withColumn("pctNearHighBy3", col("countNearHighBy3").divide(col("groupSize")))
      .withColumn("pctNearLowBy3", col("countNearLowBy3").divide(col("groupSize")))
      .withColumn("pctPriceAcc", col("countPriceAcc").divide(col("groupSize")))
      .withColumn("pctPriceDec", col("countPriceDec").divide(col("groupSize")))

    // calculate rows with the lower of each high/low percentage
    // hindenburg is calculated when BOTH #highs and #low if above a given pct
    val withHindenbergs = groupedWithPct
      .withColumn("hindenberg", when(col("pctAt52High").lt(col("pctAt52Low")),
        col("pctAt52High")).otherwise(col("pctAt52Low")))
      .withColumn("hindNearBy3", when(col("pctNearHighBy3").lt(col("pctNearLowBy3")),
        col("pctNearHighBy3")).otherwise(col("pctNearLowBy3")))
      .withColumn("hindNearBy5", when(col("pctNearHighBy5").lt(col("pctNearLowBy5")),
        col("pctNearHighBy5")).otherwise(col("pctNearLowBy5")))
      .withColumn("hindPriceMovement", when(col("pctPriceAcc").lt(col("pctPriceDec")),
        col("pctPriceAcc")).otherwise(col("pctPriceDec")))
      .orderBy("Date")

    val _10days = Window.rowsBetween(-10, 0)

    // add smoothing to the hindenberg columns by taking a 10 day moving average
    val Results = withHindenbergs
      .withColumn("smoothed_hindenberg", mean("hindenberg").over(_10days))
      .withColumn("smoothed_hindNearBy3", mean("hindNearBy3").over(_10days))
      .withColumn("smoothed_hindNearBy5", mean("hindNearBy5").over(_10days))
      .withColumn("smoothed_priceMovement", mean("hindPriceMovement").over(_10days))
    Results
      .withColumn("Date", date_format(col("Date"), "MM-dd-yyyy"))
      .coalesce(1)
      .write
      .option("header", "true")
      .csv(args(1))
  }
}