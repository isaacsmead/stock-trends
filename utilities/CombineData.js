const fs = require ('fs');

if (process.argv.length < 4){
    console.error("args <<input dir>> <<output file>>");
    exit(1)
}

const inputDir = process.argv[2];
const outputFile = process.argv[3];

if(fs.exists(outputFile)){
    console.error(`Outfile ${outputFile} already exists`);
    exit(1)
}

const out = fs.openSync(outputFile, 'w+');

let addedHeader = false;
for (const dir of fs.readdirSync(inputDir)){
    if(dir.endsWith('.txt')){
        const id = dir.replace(".txt", "");
        const lines = fs.readFileSync(`${inputDir}/${dir}`, 'utf-8')
            .split('\n').filter(Boolean);
        for(const idx of lines.keys()){
            if(!addedHeader){
                fs.writeSync(out, `id,${lines[idx]}\n`);
                addedHeader = true;
            }
            else if (idx !== 0){
                fs.writeSync(out, `${id},${lines[idx]}\n`)
            }
        }
    }
}

fs.close(out);