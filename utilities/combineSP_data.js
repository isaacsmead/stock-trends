const fs = require ('fs');

if (process.argv.length < 4){
    console.error("args <<input dir>> <<output file>>");
    exit(1)
}

inputDir = process.argv[2];
outputFile = process.argv[3];

if(fs.exists(outputFile)){
    console.error(`Outfile ${outputFile} already exists`);
    exit(1)
}

const out = fs.openSync(outputFile, 'w+');

for (const dir of fs.readdirSync(inputDir)){
    fs.writeSync(out,"id,Date,OpenInt,Open,High,Low,Close,Volume\n");
    if(dir.endsWith('.csv')){
        let id = dir.replace(".csv", "");
        id = id.replace("table_","");
        const lines = fs.readFileSync(`${inputDir}/${dir}`, 'utf-8')
            .split('\n').filter(Boolean);
        for(const line of lines){
            const fields = line.split(",");
            const date = `${fields[0].slice(0,4)}-${fields[0].slice(4,6)}-${fields[0].slice(6)}`;
            const theRest = fields.slice(1);
            fs.writeSync(out, `${id},${date},${theRest}\n`)
        }
    }
}

fs.close(out);